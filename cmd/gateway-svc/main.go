package main

import (
	"gateway-svc/internal"
)

func main() {
	internal.InitializeAndStart()
}
