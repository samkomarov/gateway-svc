package internal

import (
	"log"
	"net/http"
)

const proxyConfigPath = "proxy_config.json"
const httpHost = ":8080"

func InitializeAndStart() {
	proxyCfg := ReadProxyConfig(proxyConfigPath)
	epFactory := NewEndpointFactory()
	router := NewProxyRouter(proxyCfg, epFactory)
	log.Printf("Listening at %s", httpHost)
	log.Print(http.ListenAndServe(httpHost, router))
}
