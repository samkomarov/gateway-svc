package internal

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
	"net/http/httputil"

	"strings"
)

func NewProxyRouter(cfg ProxyConfig, newEndpoint EndpointFactory) http.Handler {
	router := chi.NewRouter()
	for i := range cfg {
		ep := &cfg[i]
		router.Method(strings.ToUpper(ep.Method), ep.Path, newEndpoint(ep))
	}
	return router
}

type EndpointFactory func(ep *EndpointConfig) http.Handler

func NewEndpointFactory() EndpointFactory {
	return func(ep *EndpointConfig) http.Handler {
		proxy := &httputil.ReverseProxy{
			Director: newRequestModifier(ep),
			ErrorHandler: func(w http.ResponseWriter, r *http.Request, err error) {
				w.WriteHeader(http.StatusBadGateway)
				log.Printf("unable to forward a request from %s to %s", r.URL, ep.Proxy.Host)
				w.Write([]byte(fmt.Sprintf("looks like service at %s is unavailable", ep.Proxy.Host))) // nolint
			},
		}
		return proxy
	}
}

func newRequestModifier(ep *EndpointConfig) func(*http.Request) {
	paramNames := getParamNames(ep.Path)
	return func(req *http.Request) {
		req.Host = ep.Proxy.Host
		req.URL.Host = ep.Proxy.Host
		req.URL.Scheme = determineScheme(req)
		req.URL.Path = insertURLParams(paramNames, ep.Proxy.Path, req)
		log.Printf("forwarding a request to %s", req.URL)
	}
}

func getParamNames(pathTemplate string) (params []string) {
	for _, part := range strings.Split(pathTemplate, "{") {
		finish := strings.Index(part, "}")
		if finish != -1 {
			params = append(params, part[:finish])
		}
	}
	return params
}

func insertURLParams(paramNames []string, proxyTemplate string, r *http.Request) (filledPath string) {
	filledPath = proxyTemplate
	for _, param := range paramNames {
		filledPath = strings.ReplaceAll(filledPath, "{"+param+"}", chi.URLParam(r, param))
	}

	return filledPath
}

// Since strangely req.URL.Scheme is always empty, you need this hack for the proxy to work
// refer to https://github.com/golang/go/issues/28940
// Possible workaround: providing scheme in the proxy config itself
func determineScheme(req *http.Request) string {
	if req.TLS != nil {
		return "https"
	}
	return "http"
}
