package internal

import (
	"encoding/json"
	"fmt"
	"os"
)

type ProxyConfig []EndpointConfig

type EndpointConfig struct {
	Path   string       `json:"path"`
	Method string       `json:"method"`
	Proxy  ProxyForward `json:"proxy"`
}

type ProxyForward struct {
	Host string `json:"host"`
	Path string `json:"path"`
}

func ReadProxyConfig(filePath string) ProxyConfig {
	file, err := os.Open(filePath)
	if err != nil {
		panic(fmt.Errorf("while opening proxy config file %s: %v", filePath, err))
	}
	defer file.Close()

	var config ProxyConfig
	if err := json.NewDecoder(file).Decode(&config); err != nil {
		panic(fmt.Errorf("while parsing config file %s: %v", filePath, err))
	}

	return config
}
